package com.diandian.sdk;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.diandian.sdk.core.openApi.DDSdk;
import com.diandian.sdk.core.openApi.SDKInterface;
import com.diandian.sdk.core.param.OrderParams;
import com.diandian.sdk.core.util.LogUtil;


public class MainActivity extends AppCompatActivity {


    private Button loginBt;
    private Button payBt;
    private Button logoutBt;


    //Valiant Force cn
    private static final int TEST_APP_ID = 2066;
    private static final String TEST_APP_KEY = "e7dfe91444560ff0fcb1b5fac3c1877e";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        DDSdk.getInstance().onCreate(this);
        initView();
    }

    private void initView(){

        logoutBt = (Button)findViewById(R.id.logout);

        logoutBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!DDSdk.getInstance().ddOpenCheckLogin()){
                    Toast.makeText(MainActivity.this,"未登录",Toast.LENGTH_SHORT).show();
                    return;
                }

                DDSdk.getInstance().ddOpenLogOut();
            }
        });

        loginBt = (Button)findViewById(R.id.login);
        loginBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                DDSdk.getInstance(MainActivity.this).ddOpenLogin(new SDKInterface.LoginCallBack() {
//                    @Override
//                    public void succeed(String uid, String Token, String msg) {
//
//                        Toast.makeText(MainActivity.this,"login success:"+uid+"--"+Token+"---"+msg,Toast.LENGTH_SHORT).show();
//
//                    }
//
//                    @Override
//                    public void failed(String msg) {
//                        Toast.makeText(MainActivity.this,"login failed :"+msg,Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void cancelled() {
//                        Toast.makeText(MainActivity.this,"cancelled",Toast.LENGTH_SHORT).show();
//                    }
//                });
//                DDLoginLib.setAppInfo(String.valueOf(TEST_APP_ID),TEST_APP_KEY);
//                DDLoginLib.setDebug(true);
//                DDLoginLib.setActivity(MainActivity.this);
//                DDLoginLib.DDPhoneLogin(new DDLoginLib.DDPhoneLoginCallback() {
//                    @Override
//                    public void onSuccess(UserSession userSession) {
//                        Toast.makeText(MainActivity.this,"login success:"+userSession.toString(),Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onFail(int i, String s) {
//                        Toast.makeText(MainActivity.this,"login fail:"+s.toString(),Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onCancel() {
//                        Toast.makeText(MainActivity.this,"login cancel",Toast.LENGTH_SHORT).show();
//                    }
//                },false);
            }
        });
        payBt = (Button)findViewById(R.id.pay);
        payBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String orderNumber = "" + System.currentTimeMillis();
                LogUtil.v("suiyi", "GAME ORDER ID :" + orderNumber);
                OrderParams orderParams = new OrderParams();
                setParamValues(orderNumber,1,orderParams);
                DDSdk.getInstance().ddOpenPay(orderParams, new SDKInterface.PayCallBack() {
                    @Override
                    public void succeed(String commonOrderId, String msg) {
                        LogUtil.e("suiyi",commonOrderId);
                        Toast.makeText(MainActivity.this,"ddOpenPay success:"+commonOrderId+"----"+msg,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failed( String msg) {
                        Toast.makeText(MainActivity.this,"ddOpenPay failed:"+msg,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void cancelled(String msg) {
                        Toast.makeText(MainActivity.this,"ddOpenPay cancelled:"+msg,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void ordered(String msg) {
                        Toast.makeText(MainActivity.this,"ddOpenPay ordered:"+msg,Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

    }

    private void setParamValues(String orderNumber, int price, OrderParams orderParams) {
        /** 游戏传入的外部订单号，每笔订单请保持订单号唯一；String */
        /**
         * appication's order serial number,please be unique for each request to
         * doPay() method;String
         */
        orderParams.setOrderNum(orderNumber);
        /**
         * 金额，以分为单位；建议传入100分的整数倍，因为有些渠道(例如百度多酷、酷狗)以元为单位，若传入金额不是100分的整数倍，
         * 则这些渠道包无法支付；int
         */
        /**
         * price,unit is RMB Fen. some channel's paying unit is RMB Yuan,but
         * OneSDK's is RMB Fen,so you'd better set price to An integer multiple
         * of 100;int
         */
        orderParams.setPrice(String.valueOf(price));
        /** 游戏服务器id，默认为0；int */
        /**
         * game server id,in sdk developer center, you can config a payment
         * notify url for each server id(1 notify url,1 server id);int
         */
        orderParams.setServerId(1);

        /** 游戏币与人民币兑换比率,例如100为1元人民币兑换100游戏币；int */
        /**
         * exchange rate between game currency and RMB Yuan,for example:100
         * means 1 RMB Yuan could rate 100 game currency;int
         */
        orderParams.setExchangeRate(100);
        /** 商品id；String */
        /**
         * product Id;String
         */
        orderParams.setProductId("1");
        /** 商品名称；String */
        /**
         * 由于"益玩渠道"商品名称显示的特殊性，因此增加渠道判断，如果渠道号为51则特殊处理商品名称
         */
        /** product name;String */
        /**
         * because of the channel "ewan" is different from the others,so if
         * channelId is 51,you need code specially like follows when set
         * productName
         */
//        int channelId = DDSdk.getInstance(this).getChannelId();
//        if (channelId == 51 || channelId == 86) {
//            orderParams.setProductCount(1000);
//            orderParams.setProductName("元宝礼包");
//        } else {
//        }
        orderParams.setProductName("1000元宝礼包");
        /** 商品描述；String */
        /** product description;String */
        orderParams.setProductDesc("1000元宝礼包的商品描述");
        /** 附加字段；放在附加字段中的值，OneSDK服务器端会不作任何修改通过服务器通知透传给游戏服务器；String */
        /**
         * extra data,sdk server will pass 'extra data' via sdk server notify
         * with modify nothing;String
         */
        //orderParams.setExt("附加字段");
        /** 用户游戏币余额，如果没有账户余额，请填0;String */
        /**
         * user game currency balance,if your game doesn't have this params set
         * 0(zero) please;String
         */
        orderParams.setBalance("1000");
        /** vip等级，如果没有，请填0；String */
        /**
         * user vip level,if your game doesn't have this params set 0(zero)
         * please;String
         */
        orderParams.setVip("vip0");
        /** 角色等级，如果没有，请填0；String */
        /**
         * user role level,if your game doesn't have this params set 0(zero)
         * please;String
         */
        orderParams.setLv("20");
        /** 工会、帮派名称，如果没有，请填0；String */
        /**
         * party(a faction of users) name,if your game doesn't have this params
         * set 0(zero) please;String
         */
        orderParams.setPartyName("鏖战群雄帮会");
        /** 角色名称；String */
        /**
         * role name;String
         */
        orderParams.setRoleName("meteor");
        /** 角色id；String */
        /**
         * role id;String
         */
        orderParams.setRoleId("123456");
        /** 所在服务器名称；String */
        /**
         * server name which the role in;String
         */
        orderParams.setServerName("1区服务器");
        /** 公司名称；String */
        /**
         * company name;String
         */
        orderParams.setCompany("pwrd");
        /** 货币名称；String */
        /**
         * game currency name;String
         */
        orderParams.setCurrencyName("元宝");

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        DDSdk.getInstance().onBackPressed(new SDKInterface.ExitCallBack() {
            @Override
            public void onChannelHasExitView() {

            }

            @Override
            public void onGameHasExitView() {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        DDLoginLib.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        Log.e("suiyi","onSaveInstanceState");
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.e("suiyi","onSaveInstanceState111");
        super.onSaveInstanceState(outState);
    }
}